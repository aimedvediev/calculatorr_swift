//
//  ViewController.swift
//  calculator
//
//  Created by Anton Medvediev on 17/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var numberLabel: UILabel!
    
    private var isNextValue: Bool = true
    
    private var valueCalc: (value1: Double, calcMetchod: String)?
    
    private var numberValue: Double{
        get{
            return Double(numberLabel.text!)!
        }
        set{
            numberLabel.text = String(newValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonCalc(_ sender: UIButton) {
        isNextValue = true
        
        if sender.currentTitle == "%"{
            numberValue = numberValue/100
        }else if sender.currentTitle == "Del"{
            //numberLabel.text!.removeLast()
            numberValue = 0
        }else if sender.currentTitle == "."{
            isNextValue = false
            if !(numberLabel.text?.contains("."))!{
                numberLabel.text?.append(".")
            }
        }
        else if sender.currentTitle == "="{
            numberValue = performCalc(value2:numberValue)!
        }else{
            valueCalc = (value1:numberValue, calcMetchod: sender.currentTitle!)
            
        }
    }
    
    func performCalc(value2: Double) -> Double?{
        if let value1 = valueCalc?.value1, let operation = valueCalc?.calcMetchod {
            switch operation {
            case "+":
                return value1+value2
            case "-":
                return value1-value2
            case "X":
                return value1*value2
            case "/":
                return value1/value2
            default:
                fatalError("error")
            }
        }
        return nil
    }
    
    @IBAction func numberButtonCalc(_ sender: UIButton) {
        if let number = sender.currentTitle{
            
            if isNextValue{
                numberLabel.text=number
                isNextValue = false
            }else{
                numberLabel.text! += "\(number)"
            }
        }
    }
}

